#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    db =QSqlDatabase::addDatabase("QODBC");
    db.setDatabaseName("DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};FIL={MS Access};DBQ=C:/Users/mekmu/Desktop/Test web/test program/POS/database.accdb");
    if(db.open()){
        qDebug() <<"Connected!";
    }else{
        qDebug() <<db.lastError();
    }
    model = new QSqlTableModel(this,db);
    model->setTable("Store Status");
    model->select();

    ui->tableView->setModel(model);
}

MainWindow::~MainWindow()
{
    delete ui;
}

